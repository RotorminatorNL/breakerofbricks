using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Paddle : MonoBehaviour
{
    private Image paddleImage;

    private void Awake()
    {
        paddleImage = GetComponent<Image>();
    }

    private void Update()
    {
        GameObject levelBackground = gameObject.transform.parent.gameObject;
        Rect levelBackgroundRect = levelBackground.GetComponent<RectTransform>().rect;

        float halfPaddleWidth = GetComponent<RectTransform>().rect.width * transform.localScale.x / 2 * GameplayManager.Instance.GetCurrentCanvasScale();
        float halfLevelBackgroundWidth = levelBackgroundRect.width / 2 * GameplayManager.Instance.GetCurrentCanvasScale();

        float leftWallPos = levelBackground.transform.position.x + halfPaddleWidth - halfLevelBackgroundWidth;
        float rightWallPos = levelBackground.transform.position.x - halfPaddleWidth + halfLevelBackgroundWidth;

        if (leftWallPos < Input.mousePosition.x && rightWallPos > Input.mousePosition.x)
        {
            transform.position = new Vector2(Input.mousePosition.x, transform.position.y);
        }
        else if (leftWallPos >= Input.mousePosition.x)
        {
            transform.position = new Vector2(leftWallPos, transform.position.y);
        }
        else
        {
            transform.position = new Vector2(rightWallPos, transform.position.y);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        try
        {
            collision.gameObject.GetComponent<LimitedEffect>().Collected();
        }
        catch (System.Exception)
        {
            return;
        }
    }

    public void DecreasePaddleLength(bool value)
    {
        if (value)
        {
            transform.localScale = new Vector3(0.5f, 1f);
            paddleImage.pixelsPerUnitMultiplier = 5.5f;
        }
        if (!value && transform.localScale.x < Vector3.one.x) ResetPaddleLength();
    }

    public void IncreasePaddleLength(bool value)
    {
        if (value)
        {
            transform.localScale = new Vector3(1.5f, 1f);
            paddleImage.pixelsPerUnitMultiplier = 12f;
        }
        if (!value && transform.localScale.x > Vector3.one.x) ResetPaddleLength();
    }

    private void ResetPaddleLength()
    {
        if (transform.localScale != Vector3.one)
        {
            transform.localScale = Vector3.one;
            paddleImage.pixelsPerUnitMultiplier = 11f;
        }
    }
}
