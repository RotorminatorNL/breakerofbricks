using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitedEffect : MonoBehaviour
{
    [SerializeField] private LimitedEffectBase limitedEffectBase;

    private CircleCollider2D bcCol;
    private bool isDropping;

    public LimitedEffectBase LimitedEffectBase { get { return limitedEffectBase; } }

    private void Awake()
    {
        bcCol = GetComponent<CircleCollider2D>();
    }

    private void Update()
    {
        if (!isDropping) return;

        transform.position = new Vector2(transform.position.x, transform.position.y - GameplayManager.Instance.GetCurrentCanvasScale());
    }

    public void Dropped(Vector2 newPos)
    {
        transform.SetParent(LimitedEffectManager.Instance.GetLimitedEffectParent());
        transform.localScale = Vector2.one;
        transform.position = newPos;
        isDropping = true;
    }

    public void Collected()
    {
        isDropping = false;
        ObjectPooler.Instance.ReturnPoolObject(limitedEffectBase.Key, gameObject);
        LimitedEffectManager.Instance.ActivateLimitedEffect(limitedEffectBase);
    }

    public void NotCollected()
    {
        isDropping = false;
        ObjectPooler.Instance.ReturnPoolObject(limitedEffectBase.Key, gameObject);
    }
}
