using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LimitedEffect", menuName = "Limited Effect/Create a new limited effect")]
public class LimitedEffectBase : ScriptableObject
{
    [SerializeField] private string key;
    [SerializeField] private int timeActive;
    [SerializeField] private float timeLeft;
    [SerializeField] private bool activated;

    public string Key { get { return key; } }
    public int TimeActive { get { return timeActive; } }
    public float TimeLeft { get { return timeLeft; } }
    public bool Activated { get { return activated; } }

    public void SetActive(bool value)
    {
        activated = value;
    }

    public void SetTimeLeft(float time)
    {
        timeLeft = time;
    }
}
