using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour
{
    [SerializeField] private float ballSpeed = 200f;
    [SerializeField] private float ballStartHeight = 200f;
    [SerializeField] private Color superBallColor;

    private GameplayManager gameplayManager;
    private LimitedEffectBase[] limitedEffects;

    private CircleCollider2D colBall;
    private Rigidbody2D rbBall;
    private Image ballImage;
    private int amountBricksHit;

    public bool SuperBall { get; private set; }

    private void Awake()
    {
        colBall = GetComponent<CircleCollider2D>();
        rbBall = GetComponent<Rigidbody2D>();
        ballImage = GetComponent<Image>();
    }

    private void Start()
    {
        gameplayManager = GameplayManager.Instance;
        limitedEffects = LimitedEffectManager.Instance.LimitedEffects;

        Invoke(nameof(SetRandomDirection), 1f);
    }

    private void SetRandomDirection()
    {
        Vector2 force = Vector2.zero;
        force.x = 10f;
        force.y = 150f;

        rbBall.AddForce(force * ballSpeed * gameplayManager.GetCurrentCanvasScale());
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject collidedObj = collision.gameObject;

        if (collidedObj.name == "Paddle" || collidedObj.name == "BottomWall")
            amountBricksHit = 0;

        if (collidedObj.name.Contains("Brick"))
        {
            if (SuperBall)
            {
                gameplayManager.UpdateScore(amountBricksHit + collidedObj.GetComponent<Brick>().BrickLives);
            }
            else
            {
                gameplayManager.UpdateScore(++amountBricksHit);
            }
            DropLimitedEffectHandler();
        }
    }
    private void DropLimitedEffectHandler()
    {
        if (!WillLimitedEffectDrop())
            return;

        GameObject limitedEffect = ObjectPooler.Instance.GetPoolObject(GetRandomLimitedEffect().Key);
        Physics2D.IgnoreCollision(GetComponent<CircleCollider2D>(), limitedEffect.GetComponent<CircleCollider2D>());
        limitedEffect.GetComponent<LimitedEffect>().Dropped(transform.position);
    }

    private bool WillLimitedEffectDrop()
    {
        return Random.Range(1, 4) == 3;
    }

    private LimitedEffectBase GetRandomLimitedEffect()
    {
        return limitedEffects[Random.Range(0, limitedEffects.Length)];
    }

    public void DecreaseBallSize(bool value)
    {
        if (value) transform.localScale = new Vector3(0.5f, 0.5f);
        if (!value && transform.localScale.x < Vector3.one.x) ResetBallSize();
    }

    public void IncreaseBallSize(bool value)
    {
        if (value) transform.localScale = new Vector3(1.5f, 1.5f);
        if (!value && transform.localScale.x > Vector3.one.x) ResetBallSize();
    }

    private void ResetBallSize()
    {
        if (transform.localScale != Vector3.one) transform.localScale = Vector3.one;
    }

    public void ActiveSuperBall(bool value)
    {
        SuperBall = value;
        ballImage.color = value ? superBallColor : Color.white;
    }

    public void ResetBall()
    {
        rbBall.velocity = Vector2.zero;
        transform.localScale = Vector2.one;

        float newXPos = Screen.width / 2;
        float newYPos = gameplayManager.GetYPosLevelBackground() + ballStartHeight * gameplayManager.GetCurrentCanvasScale();
        transform.position = new Vector2(newXPos, newYPos);

        Invoke(nameof(SetRandomDirection), 1f);
    }
}
