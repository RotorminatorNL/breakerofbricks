using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfBounce : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject collidedObj = collision.gameObject;
        if (collidedObj.name == "Ball") GameplayManager.Instance.OutOfBounce();
        if (collidedObj.name.Contains("(Clone)")) collidedObj.GetComponent<LimitedEffect>().NotCollected();
    }
}
