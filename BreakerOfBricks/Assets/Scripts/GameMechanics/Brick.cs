using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Brick : MonoBehaviour
{
    [Range(1,5)]
    [SerializeField] private int brickLives = 1;
    [SerializeField] private Sprite[] brickImages;

    private Ball ballScript;
    private BoxCollider2D bcBrick;
    private Image brickImage;

    public int BrickLives { get { return brickLives; } }

    private void Awake()
    {
        bcBrick = GetComponent<BoxCollider2D>();
        brickImage = GetComponent<Image>();
        brickImage.sprite = brickImages[brickLives - 1];
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (ballScript == null) ballScript = collision.gameObject.GetComponent<Ball>();

        if (ballScript.SuperBall || brickLives == 1)
        {
            brickLives = 1;
            bcBrick.enabled = false;
            brickImage.color = new Color(0f, 0f, 0f, 0f);
        }
        else
        {
            brickImage.sprite = brickImages[--brickLives - 1];
        }
    }
}
