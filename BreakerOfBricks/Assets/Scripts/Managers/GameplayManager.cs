using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
    [SerializeField] private GameObject levelCanvas;
    [SerializeField] private GameObject levelBackground;
    [SerializeField] private GameObject ball;

    [Header("Lives")]
    [SerializeField] private int lives = 3;
    [SerializeField] private Image[] livesImages;

    [Header("Score")]
    [SerializeField] private TextMeshProUGUI scoreIndicatorText;
    [SerializeField] private float points = 100;
    private int score;

    private LimitedEffectManager limitedEffectManager;
    private RectTransform levelCanvasRectTransform;
    private RectTransform levelBackgroundRectTransform;
    private Ball ballScript;

    public static GameplayManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null) 
            Instance = this;
        else 
            Destroy(gameObject);

        limitedEffectManager = LimitedEffectManager.Instance;
        levelCanvasRectTransform = levelCanvas.GetComponent<RectTransform>();
        levelBackgroundRectTransform = levelBackground.GetComponent<RectTransform>();
        ballScript = ball.GetComponent<Ball>();

        scoreIndicatorText.text = score.ToString();
    }

    public float GetCurrentCanvasScale()
    {
        return levelCanvasRectTransform.localScale.x;
    }

    public float GetYPosLevelBackground()
    {
        return levelBackground.transform.position.y - levelBackgroundRectTransform.rect.height * GetCurrentCanvasScale() / 2;
    }

    public void OutOfBounce()
    {
        livesImages[--lives].gameObject.SetActive(false);

        if (lives > 0)
            ResetLevel();
        else
            GameOver();
    }

    private void ResetLevel()
    {
        ballScript.ResetBall();
        limitedEffectManager.RemoveAllDroppingLimitedEffects();
        limitedEffectManager.DeactivateLimitedEffects();
    }

    private void GameOver()
    {
        Debug.Log("Game over!!!");
        ball.SetActive(false);
    }

    public void UpdateScore(float bricksHit)
    {
        if (bricksHit == 1) score += (int)(points * bricksHit);
        else score += (int)(points * (1f + bricksHit / 10f));

        scoreIndicatorText.text = score.ToString();
    }
}
