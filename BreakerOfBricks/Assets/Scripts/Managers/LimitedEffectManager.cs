using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitedEffectManager : MonoBehaviour
{
    [SerializeField] private GameObject ball;
    [SerializeField] private GameObject paddle;

    [Header("Limited Effects")]
    [SerializeField] private Transform limitedEffectsParent;
    [SerializeField] private LimitedEffectBase[] limitedEffects;

    private Ball ballScript;
    private Paddle paddleScript;

    public LimitedEffectBase[] LimitedEffects { get { return limitedEffects; } }
    public static LimitedEffectManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null) Instance = this; 
        else Destroy(gameObject);

        ballScript = ball.GetComponent<Ball>();
        paddleScript = paddle.GetComponent<Paddle>();

        DeactivateLimitedEffects();
    }

    private void Update()
    {
        for (int i = 0; i < limitedEffects.Length; i++)
        {
            LimitedEffectBase limitedEffect = limitedEffects[i];
            switch (limitedEffect.Key)
            {
                case "DecreaseBallSize":
                    if (limitedEffect.Activated) ActiveLimitedEffectHandler(limitedEffect);
                    else ballScript.DecreaseBallSize(false);
                    break;
                case "DecreasePaddleLength":
                    if (limitedEffect.Activated) ActiveLimitedEffectHandler(limitedEffect);
                    else paddleScript.DecreasePaddleLength(false);
                    break;
                case "IncreaseBallSize":
                    if (limitedEffect.Activated) ActiveLimitedEffectHandler(limitedEffect);
                    else ballScript.IncreaseBallSize(false);
                    break;
                case "IncreasePaddleLength":
                    if (limitedEffect.Activated) ActiveLimitedEffectHandler(limitedEffect);
                    else paddleScript.IncreasePaddleLength(false);
                    break;
                case "SuperBall":
                    if (limitedEffect.Activated) ActiveLimitedEffectHandler(limitedEffect);
                    else ballScript.ActiveSuperBall(false);
                    break;
            }
        }
    }

    public Transform GetLimitedEffectParent()
    {
        return limitedEffectsParent;
    }

    public void ActivateLimitedEffect(LimitedEffectBase limitedEffect)
    {
        if (!limitedEffect.Activated)
        {
            switch (limitedEffect.Key)
            {
                case "DecreaseBallSize":
                    ballScript.DecreaseBallSize(true);
                    break;
                case "DecreasePaddleLength":
                    paddleScript.DecreasePaddleLength(true);
                    break;
                case "IncreaseBallSize":
                    ballScript.IncreaseBallSize(true);
                    break;
                case "IncreasePaddleLength":
                    paddleScript.IncreasePaddleLength(true);
                    break;
                case "SuperBall":
                    ballScript.ActiveSuperBall(true);
                    break;
            }

            limitedEffect.SetActive(true);
            limitedEffect.SetTimeLeft(limitedEffect.TimeActive);
        }
        else
        {
            limitedEffect.SetTimeLeft(limitedEffect.TimeActive);
        }
    }

    private void ActiveLimitedEffectHandler(LimitedEffectBase limitedEffect)
    {
        if (limitedEffect.TimeLeft > 0)
        {
            limitedEffect.SetTimeLeft(limitedEffect.TimeLeft - Time.deltaTime);
        }
        else
        {
            limitedEffect.SetActive(false);
            limitedEffect.SetTimeLeft(0);
        }
    }

    public void DeactivateLimitedEffects()
    {
        foreach (LimitedEffectBase effect in limitedEffects)
        {
            effect.SetActive(false);
            effect.SetTimeLeft(0);
        }
    }

    public void RemoveAllDroppingLimitedEffects()
    {
        for (int i = 0; i < limitedEffectsParent.childCount; i++)
        {
            GameObject limitedEffect = limitedEffectsParent.GetChild(i).gameObject;
            LimitedEffectBase limitedEffectBase = limitedEffect.GetComponent<LimitedEffect>().LimitedEffectBase;

            ObjectPooler.Instance.ReturnPoolObject(limitedEffectBase.Key, limitedEffect);
            i--;
        }
    }
}
