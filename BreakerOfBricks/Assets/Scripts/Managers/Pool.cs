using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Pool
{
    public Transform PoolParent;
    public string Key;
    public GameObject Prefab;
    public int Amount;
}
