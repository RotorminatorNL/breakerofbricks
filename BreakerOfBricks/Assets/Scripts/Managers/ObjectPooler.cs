using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [SerializeField] private List<Pool> pools;

    private Dictionary<string, Queue<GameObject>> poolDictionary;
    private Dictionary<string, GameObject> prefabDictionary;
    private Dictionary<string, Transform> parentDictionary;

    public static ObjectPooler Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(gameObject);

        InitiateDictionaries();
    }

    private void InitiateDictionaries()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();
        prefabDictionary = new Dictionary<string, GameObject>();
        parentDictionary = new Dictionary<string, Transform>();

        foreach (Pool pool in pools)
        {
            poolDictionary.Add(pool.Key, GetPoolObjects(pool));
            prefabDictionary.Add(pool.Key, pool.Prefab);
            parentDictionary.Add(pool.Key, pool.PoolParent);
        }
    }

    private Queue<GameObject> GetPoolObjects(Pool pool)
    {
        Queue<GameObject> poolObjects = new Queue<GameObject>();

        for (int i = 0; i < pool.Amount; i++)
        {
            GameObject obj = Instantiate(pool.Prefab);
            obj.transform.SetParent(pool.PoolParent);
            obj.SetActive(false);
            poolObjects.Enqueue(obj);
        }

        return poolObjects;
    }

    public GameObject GetPoolObject(string key)
    {
        GameObject poolObject;

        if (poolDictionary[key].Count > 0)
        {
            poolObject = poolDictionary[key].Dequeue();
            poolObject.SetActive(true);
        }
        else
        {
            poolObject = Instantiate(prefabDictionary[key]);
            poolObject.transform.SetParent(parentDictionary[key]);
        }

        return poolObject;
    }

    public void ReturnPoolObject(string key, GameObject poolObject)
    {
        poolDictionary[key].Enqueue(poolObject);
        poolObject.transform.SetParent(parentDictionary[key]);
        poolObject.SetActive(false);
    }
}
